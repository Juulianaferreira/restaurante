package br.ucsal.bes20182.testequalidade.restaurante.business;

import static org.junit.Assert.assertEquals;

import java.util.Scanner;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal.bes20182.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;

public class RestauranteBOIntegradoTest {

	// M�todo a ser testado: public void incluirItemComanda(Integer
	// codigoComanda, Integer codigoItem, Integer qtdItem) throws
	// RegistroNaoEncontrado, ComandaFechadaException {
	// Verificar se a inclus�o de uma item em uma comanda fechada levanta
	// exce��o.
	@Test (expected=ComandaFechadaException.class)
	public void incluirItemComandaFechada() throws Throwable {
		Integer numeroMesa= 2;
		Mesa mesa = new Mesa(numeroMesa);
		Comanda comanda = new Comanda(mesa);
		Item item1 = new Item("Caf�", 3.50);
		comanda.situacao = SituacaoComandaEnum.FECHADA;
		
		try {
			comanda.incluirItem(item1, 2);
		} catch (ComandaFechadaException e) {
			throw e.getCause();
		}
		
	}
}
